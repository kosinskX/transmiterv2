package App.Interface.Console;

import App.FileManager.FileController;
import App.Interface.Utils;
import App.Net.Client.SocketTCP;
import App.Net.Server.ServerTCP;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Console app interface
 */
public class App {

    public static ArrayList<SocketTCP> tcpServers = new ArrayList<>();
    private ArrayList<SocketTCP> tcpTasks;

    private ServerTCP serverTCP;

    private boolean isRunning;


    public App(){

        tcpTasks = new ArrayList<>();

        isRunning = true;
        int port = Utils.getIntConsole("Podaj port serwera");
            serverTCP = new ServerTCP(port);
            serverTCP.start();

        try{
            console();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Console main loop
     */
    private void console() throws IOException {
        while (isRunning){
            String command = Utils.getText("Enter command");
            if (command.toLowerCase().equals("exit")){
                exit();
            }else  if (command.toLowerCase().matches("list files")){
                System.out.println("Lista plików -------------------------------------");
                for(File file : new File(FileController.dirPath).listFiles()){
                    if (file.isFile()){
                        System.out.print(file.getName());
                        System.out.print(" - ");
                        System.out.println(FileController.generateMD5(file));
                    }
                }


                System.out.println("Zdalne pliki -------------------------------------");
                for(SocketTCP socketTCP : tcpServers){
                    socketTCP.getFiles();
                }
                System.out.println("--------------------------------------------------");
            }else  if (command.toLowerCase().matches("list servers")){
                System.out.println("Connected servers:");
                for (int i = 0; i < tcpServers.size(); i++) {
                    System.out.println(i + ". " + tcpServers.get(i).getAddress());
                }
            }else  if (command.toLowerCase().matches("connect [0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}:[0-9]{3,4}")){
                String[] arguments = command.split(" ");


                SocketTCP newServer = SocketTCP.connect(arguments[1]);

                if (newServer != null && newServer.isConnected()){
                    tcpServers.add(newServer);
                }

            }else  if (command.toLowerCase().matches("pull [\\d]{1,5} .+")){
                String[] arguments = command.split(" ");

                SocketTCP server = tcpServers.get(Integer.parseInt(arguments[1]));

                SocketTCP task = SocketTCP.connect(server.getAddress());

                tcpTasks.add(task);
                task.setTask("pull",arguments[2]);
                task.start();

            }else  if (command.toLowerCase().matches("push [\\d]{1,5} .+")){
                String[] arguments = command.split(" ");

                SocketTCP server = tcpServers.get(Integer.parseInt(arguments[1]));

                SocketTCP task = SocketTCP.connect(server.getAddress());

                tcpTasks.add(task);
                task.setTask("push",arguments[2]);
                task.start();
            }else  if (command.toLowerCase().matches("close")){
                exit();

            }else  if (command.toLowerCase().matches("progress")){
                System.out.println("Progress TCP tasks:");
                for (int i = 0; i < tcpServers.size(); i++) {
                    System.out.println(i + ". " + tcpServers.get(i).progressTask());
                }
            }else  if (command.toLowerCase().equals("h") ||command.toLowerCase().equals("help") || command.toLowerCase().equals("?")){
                System.out.println("------- HELP ----------");
                System.out.println("list files - lista plików");
                System.out.println("list servers - lista serwerów");
                System.out.println("connect ip:port | przykład: connect 127.0.0.1:8999 - utwórz połączenie");
                System.out.println("pull nrSerwera nazwaPliku - ściąganie pliku");
                System.out.println("push nrSerwera nazwaPliku - wysyłanie pliku");
                System.out.println("exit - zamknięcie");
                System.out.println("------- HELP ----------");

            }else{
                System.out.println("Wrong command!");
            }
        }

    }

    private void exit() {
        System.out.println("Close App");
        isRunning = false;
        try{

            for (SocketTCP s : tcpServers){
                s.close();
            }

            for (SocketTCP s : tcpTasks){
                s.close();
            }

            if (serverTCP != null){
                serverTCP.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.exit(0);
    }


    private void tryDownloadAgain(){

    }
}
