package App.Interface;

import java.util.Scanner;

public class Utils {

    private static final String pipe = " $:";
    private static final Scanner  s = new Scanner(System.in);

    private static final String msgWrongCommand = "Wrong command, try again!";

    public static int getIntConsole(String msg){

        try{
            System.out.print(msg + pipe);
            return Integer.parseInt(s.nextLine());
        }catch (Exception e){
            return getIntConsole(msg);
        }
    }

    public static String getCommandConsole(String msg, String regexCommand){
        System.out.print(msg + pipe);
        String command = s.nextLine();
        if (command.matches(regexCommand)){

            return command;
        }else{
            System.out.println(msgWrongCommand);
            return getCommandConsole(msg, regexCommand);
        }
    }

    public static String getText(String msg){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String command = "";
        try {
            System.out.print(msg + pipe);
            command = s.nextLine();
        }catch (Exception e){
           return getText(msg);
        }
        return command;
    }
}
