package App;

import App.FileManager.FileController;

/**
 * Starting point
 */
public class Transmiter {

    public static void main(String[] args) {
        FileController.createDirectory();
        new App.Interface.Console.App();
    }
}
