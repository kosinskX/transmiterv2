package App.FileManager;


import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.util.ArrayList;

/**
 * Manager for read/write files
 */
public class FileController {

    private static final String configPath = "./config.json";
    public static final String dirPath = "./data";

    public static final int maxPackage = 125;

    /**
     * @return Return true if config file exists
     */
    public static boolean configExist() {
        return new File(configPath).exists();
    }


    public static String generateMD5(File file) {
        try{
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hashByte =  md.digest(Files.readAllBytes(file.toPath()));


            StringBuilder sb = new StringBuilder();
            for(byte b : hashByte){
                sb.append(String.format("%02x", b));
            }

            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static long getCurrentSize(String f){
        try {
            return new RandomAccessFile(dirPath + "/" +f, "r").length();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static boolean fileExist(String s){
        try{
            File dir = new File(dirPath);
            if (dir.exists())
                for(File f : dir.listFiles()){
                    if (f.getName().toLowerCase().equals(s.toLowerCase())){
                        return true;
                    }
                }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    public static byte[] ofsetReadFile(File file, long ofset, int length){
        try {
            RandomAccessFile rfile = new RandomAccessFile(dirPath + "/" +file, "r");
            rfile.seek(ofset);
            byte[] data;

            if(ofset+length > rfile.length()){
                data = new byte[(int)(rfile.length() - ofset)];
            }else{
                data = new byte[length];
            }

            rfile.read(data);
            rfile.close();
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void ofsetWriteFile(String file, byte[] data){
        try{
            RandomAccessFile rfile = new RandomAccessFile(dirPath + "/" +file, "rw");
            long ofset = rfile.length()-1 > 0 ? rfile.length()-1 : 0;
            rfile.seek(ofset);
            rfile.write(data);
            rfile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createDirectory(){
        File dir = new File(dirPath);

        if (!dir.exists()){
            dir.mkdir();
        }
    }

    public static byte[] rewritePackage(int operation, byte[] data){
        int length = data.length+2;
        byte[] newData = new byte[length];
        newData[0] = (byte)operation;
        newData[1] = (byte) data.length;
        for (int i = 2; i < newData.length; i++) {
            newData[i] = data[i-2];
        }
        return newData;
    }

    public static byte[] removeMeta(byte[] data) {
        int size = data[1];
        byte[] newData = new byte[size];

        for (int i = 2; i < size+2; i++) {
            newData[i-2] = data[i];
        }
        return newData;
    }
}
