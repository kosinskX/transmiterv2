package App.Net.Server;

import App.Net.Client.SocketTCP;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerTCP extends Thread{

    private ServerSocket serverSocket;
    private boolean isRunning;


    public ServerTCP(int port){
        try {
            isRunning = true;
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Nie można utworzyć serwera");
            System.exit(1);
        }
    }


    @Override
    public void run() {
        server();
    }


    private void server(){
        while (isRunning){
            try {
                System.out.println("Nasłuch");
                Socket s = serverSocket.accept();

             SocketTCP stcp =   SocketTCP.connect(s);
             stcp.start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        close();
    }

    public void close() {
        isRunning = false;
        try {
            serverSocket.close();
        } catch (IOException e) {

        }
    }
}
