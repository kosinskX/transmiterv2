package App.Net;

import java.io.IOException;
import java.net.Socket;
import java.util.Random;

/**
 * Independents methods for network communication
 */
public class Utils {

    // Transmition codes
    public static final int OPERATION_REFUSE                    = -3; // Refuse for any action
    public static final int OPERATION_SUCCESS                   = -2; // Success code for any operation
    public static final int OPERATION_FAILED                    = -1; // Failed code for any operation
    public static final int CONNECTED                           =  0; // Return auth key for both connect type
    public static final int COMMAND_CONNECT                     =  1; // Connect w/o  auth code
    public static final int COMMAND_LIST                        =  2; // Return file list
    public static final int COMMAND_FILE_ONE_HOST     =  3; // Download file from other host
    public static final int COMMAND_FILE_MULTI_HOST   =  4; // Download file from other hosts in parallel
    public static final int COMMAND_FILE_MD5               =  5; // Get MD5 for selected file
    public static final int COMMAND_SYNC                   =  6; // Continue downloading after pause

    // Server port range
    public static final int MIN_PORT = 11000;
    public static final int MAX_PORT = 12000;


    /**
     * Search for free port
     * @return free port
     */
    public static int findFreePort(){
        int port = new Random().nextInt(MAX_PORT - MIN_PORT)+MIN_PORT;

        try {
            Socket socket = new Socket("127.0.0.1", port);
            if (socket.isConnected()){
                socket.close();
                return findFreePort();
            }
        } catch (IOException e) {
            return port;
        }
        return port;
    }




    // Data processing methods
}
