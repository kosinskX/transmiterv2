package App.Net.Client;

import App.FileManager.FileController;
import App.Interface.Console.App;
import App.Net.Utils;

import java.io.*;
import java.net.Socket;

public class SocketTCP extends Thread{


    private String address;
    private boolean isRunning;
    private Socket socket;
    private InputStream inboxByte;
    private OutputStream postmanByte;

    private PrintWriter postman;
    private BufferedReader inbox;

    private boolean isTask;
    private String taskType;
    private String taskArgs;
    private String partialTask;


    private SocketTCP(String address){
        this.address = address;
        String[] partsAddress = this.address.split(":");

        taskArgs = "";
        taskType = "";
        partialTask = "";
        isTask = false;

        try {
            socket = new Socket(partsAddress[0], Integer.parseInt(partsAddress[1]));
            postmanByte = socket.getOutputStream();
            inboxByte = socket.getInputStream();

            postman = new PrintWriter(postmanByte, true);
            inbox = new BufferedReader(new InputStreamReader(inboxByte));
            isRunning = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Connected");
    }


    private SocketTCP(Socket socket){
        System.out.println("Remote connect");
        this.address = "";

        taskArgs = "";
        taskType = "";
        partialTask = "";
        isTask = false;

        try {
            this.socket = socket;
            postmanByte = socket.getOutputStream();
            inboxByte = socket.getInputStream();
            postman = new PrintWriter(postmanByte, true);
            inbox = new BufferedReader(new InputStreamReader(inboxByte));
            isRunning = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        do{
            if (isTask){
                runTask();
            }else {
                listen();
            }
        }while (isRunning);
    }

    private void listen() {
        while (isRunning){
            try {
                String listen = inbox.readLine();
                System.out.println(listen);

                if(listen.equals("list")){
                    String files = "";


                    File directory = new File(FileController.dirPath);
                    for (File f : directory.listFiles()){
                        if (f.isFile()){
                            files += f.getName() + "\n";
                        }
                    }

                    postman.println(files);
                }else if(listen.equals("close")){
                    close();
                }else if(listen.matches("pull .+")){
                    push(listen.split(" ")[1], false);
                }else if(listen.matches("push .+")){
                    pull(listen.split(" ")[1], false);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void runTask() {
        switch (taskType){
            case "push":
            {
                String fileName = taskArgs;
                if (FileController.fileExist(fileName)){
                    if (push(fileName, true)){
                        System.out.println("Wyslany poprawnie: " + fileName);
                    }else {
                        System.out.println("Erroe, nie wyslano pliki: " + fileName);
                    }
                }else {
                    System.out.println("Plik nie istnieje: " + fileName);
                    isRunning = false;
                }
            }
                break;
            case "pull":
            {
                String fileName = taskArgs;


                if (pull(fileName, true)){
                    System.out.println("Plik pobrany pomyślnie: " + fileName);
                }else {
                    System.out.println("Error, nie pobrano pliku: " + fileName);
                }

            }
                break;
            case "ppull":
                break;
            default:
                break;
        }
    }

    // Flaga prawda gdy chcemy ściągnąć plik
    private boolean pull(String fileName, boolean flag) {
        byte[] data = new byte[127];
        try {
            if (flag){
                // Send fileName
                postman.println("pull " + fileName);
                inboxByte.read(data);
            }
            if (data[0] == Utils.OPERATION_FAILED && flag){
                System.out.println("Plik nie istnieje.");
                return false;
            }else{
                boolean readData = true;
                do{
                    inboxByte.read(data);
                    if (data[0] == Utils.COMMAND_FILE_MD5){
                        readData = false;
                        continue;
                    }

                    if (data[0] == Utils.COMMAND_FILE_ONE_HOST){
                        data = FileController.removeMeta(data);
                        FileController.ofsetWriteFile(fileName, data);
                    }


                }while (readData);

                String md5String = inbox.readLine();// Odczyt md5 z pliku

                String newFileMd5 = FileController.generateMD5(new File(fileName));

                if (md5String.equals(newFileMd5)){
                    data = FileController.rewritePackage(Utils.OPERATION_SUCCESS, data);
                    postmanByte.write(data);
                    postmanByte.flush();
                    return true;
                }else {
                    data = FileController.rewritePackage(Utils.OPERATION_FAILED, data);
                    postmanByte.write(data);
                    postmanByte.flush();
                    return false;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    // Flag prawda gdy wysyłamy plik
    private boolean push(String fileName, boolean flag) {
        try {
            byte[] date = new byte[127];

            if(!flag){// Odpowiedź na pull
                if (FileController.fileExist(fileName)){

                    date[0] = Utils.OPERATION_SUCCESS;
                    postmanByte.write(date);
                    postmanByte.flush();


                }else {
                    date[0] = Utils.OPERATION_FAILED;
                    postmanByte.write(date);
                    postmanByte.flush();
                    return false;
                }
            }else{
                postman.println("push " + fileName);
                System.out.println("Wysyłanie...");
            }




           long sendSize = 0;
            while (sendSize < FileController.getCurrentSize(fileName)){
                byte[] data = FileController.ofsetReadFile(new File(fileName), sendSize, FileController.maxPackage);
                data = FileController.rewritePackage(Utils.COMMAND_FILE_ONE_HOST, data);
                postmanByte.write(data);
                postmanByte.flush();

                sendSize += data[1];
            }

            String md5 = FileController.generateMD5(new File(FileController.dirPath + "/" + fileName));
            byte[] md5Data = md5.getBytes();
            md5Data = FileController.rewritePackage(Utils.COMMAND_FILE_MD5, md5Data);
            postmanByte.write(md5Data);
            postmanByte.flush();



            inboxByte.read(date);

            if(date[0] == Utils.OPERATION_SUCCESS){
                return true;
            }else{
                return false;
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void setTask(String taskType, String taskArgs){
        this.taskType = taskType;
        this.taskArgs = taskArgs;
        this.isTask = true;
    }


    public void setTask(String taskType, String taskArgs, String partialTask){
        this.partialTask = partialTask;
        setTask(taskType, taskArgs);
    }

    public String getAddress() {
        return address;
    }

    public void close() throws IOException {
        isRunning = false;
        if (inboxByte != null){
            inboxByte.close();
        }
        if (postmanByte != null){
            postmanByte.close();
        }
        if (socket != null){
            socket.close();
        }
    }

    public static SocketTCP connect(String address){

        SocketTCP server = new SocketTCP(address);
        return server;
    }
    public static SocketTCP connect(Socket s){

        SocketTCP server = new SocketTCP(s);
        return server;
    }

    public int progressTask() {
        return 0;
    }

    public boolean isConnected() {
        return socket.isConnected();
    }

    public void getFiles() {
        postman.println("list");
        System.out.println("Server - " + address);
        try {
            System.out.println(inbox.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
